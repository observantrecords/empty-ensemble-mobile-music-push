# Mobile Music: Push

## Overview

Push is a piece of chance music performed with smartphones. This plugin creates a shortcode which provides the instrument for performance. Audio content must be uploaded to the WordPress site.

## Installation

Install the plugin directory under ``wp-content/plugins`` or ``wp-content/mu-plugins``.

## Usage

* Create a page template that supports the sidebar `sidebar-mobile-music-instrument`. The sidebar itself need not be laid out like an actual sidebar. This widget is intended to render JavaScript only.
* Create a page in the WordPress administration for the Mobile Music: Push instrument with the shortcode `[mobile_music_push]` as the only content.
* Select the template supporting the `sidebar-mobile-music-instrument` sidebar for your page.
* Under Appearance » Widgets, add the **Mobile Music Push** widget to the **Mobile Music Instruments** sidebar.
* Upload your audio content to the media library. The file names of your audio content must follow a specific naming convention: `(Chord number)_(Melody number)_(Variant number)_(Slug).(Extention)`, e.g. `01_06_04_ringtone.mp3`. Note the use of underscores.
* Publish the page and view it. Click the **Test** button to test if audio content is loading.

