<?php
/**
 * Entry point for WordPress Plugin
 *
 * @package ObservantRecords\WordPress\Plugins\MobileMusic\Push
 * @author Greg Bueno
 * @copyright Observant Records
 *
 * @wordpress-plugin
 * Plugin Name: Mobile Music Push
 * Plugin URI: https://bitbucket.org/observantrecords/empty-ensemble-mobile-music-push
 * Description: This custom plugin creates an interface for the composition Push.
 * Version: 1.0.1
 * Author: Greg Bueno
 * Author URI: http://vigilantmedia.com
 * License: MIT
 */

namespace ObservantRecords\WordPress\Plugins\MobileMusic\Push;

if (!function_exists( __NAMESPACE__ . '\\autoload' )) {
	function autoload( $class_name )
	{
		$class_name = ltrim($class_name, '\\');
		if ( strpos( $class_name, __NAMESPACE__ ) !== 0 ) {
			return;
		}

		$class_name = str_replace( __NAMESPACE__, '', $class_name );

		$path = plugin_dir_path(__FILE__) . '/lib' . str_replace('\\', DIRECTORY_SEPARATOR, $class_name) . '.php';

		require_once($path);
	}
}

spl_autoload_register(__NAMESPACE__ . '\\autoload');

register_activation_hook(__FILE__, array('ObservantRecords\WordPress\Plugins\MobileMusic\Push\Setup', 'activate'));
register_deactivation_hook(__FILE__, array('ObservantRecords\WordPress\Plugins\MobileMusic\Push\Setup', 'deactivate'));

Setup::init();
