<?php
namespace ObservantRecords\WordPress\Plugins\MobileMusic\Push;


/**
 * Create shortcode for Push interface.
 *
 * @since 1.0.0
 *
 * @package ObservantRecords\WordPress\Plugins\MobileMusic\Push
 * @author Greg Bueno
 * @copyright Observant Records
 */
class Shortcode
{

	/**
	 * Shortcode constructor.
	 */
	public function __construct()
	{
	}

	/**
	 * Displays user interface for Push.
	 *
	 * @since 1.0.0
	 *
	 * @param $attributes
	 * @param null $content
	 * @return string
	 */
	public static function display($attributes, $content = null ) {
		$shortcode_attributes = shortcode_atts( array(

		), $attributes );

		$output = <<< OUTPUT
<div id="one-button-instrument" class="row one-button-instrument">
	<div class="col-sm-3">
		<button class="btn btn-lg btn-primary btn-block btn-play" type="button">Play</button><br/>
		<audio class="audio-play" preload="auto">
			<source class="play-mp3" src="" type="audio/mpeg" />
			<source class="play-ogg" src="" type="audio/ogg" />
		</audio>
	</div>
</div>
<div id="instrument-settings" class="row">
	<div class="col-sm-3">
		<button class="btn btn-default btn-block btn-test" type="button">Test</button>
		<audio class="audio-test" preload="auto">
			<source class="test-mp3" src="" type="audio/mpeg" />
			<source class="test-ogg" src="" type="audio/ogg" />
		</audio>
	</div>
</div>
<div class="row">
	<div id="status" class="col-sm-12"></div>
</div>
OUTPUT;

		return $output;
	}

}