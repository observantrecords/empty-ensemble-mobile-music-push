<?php
/**
 * Copyright (c) 2017 Observant Records.
 */

/**
 * Created by PhpStorm.
 * User: gregbueno
 * Date: 11/25/17
 * Time: 5:58 PM
 */

namespace ObservantRecords\WordPress\Plugins\MobileMusic\Push\Widgets;

class Push extends \WP_Widget {

	public function __construct() {
		parent::__construct('widget_mobile_music_push', 'Mobile Music Push', array(
			'description' => 'Use this widget to initialize the audio content for Push'
		) );
	}

	public function widget( $args, $instance ) {

		// Determine which ringtones to play for each chord.
		$setlist = $this->getRingtoneSetlist();

		$setlist_debug = print_r( $setlist, true );

		$variation = "Not determined.";

		foreach ( $setlist as $prefix ) {
			if ( $prefix != "noise" ) {
				$variation = substr( $prefix, -2 );
				break;
			}
		}

		$attachments = get_posts(array(
			'post_type' => 'attachment',
			'numberposts' => -1,
			'post_mime_type' => array(
				'audio/mpeg',
				'audio/ogg',
			),
			'orderby' => 'title',
		));

		$setlist_files = $this->getSetlistFiles( $attachments, $setlist );

		$push_json = $this->filterAudioJson( $setlist_files );

		$output = <<< OUTPUT
<div class="audio-preload"></div>
<script type="text/javascript">
	var playAudio = $push_json;
	(function ($) {
		$(function () {
			var audioPreload = $('.audio-preload');
			for (var p in playAudio) {
				var audio = $('<audio>');
				var sourceMp3 = $('<source>');
				
				sourceMp3.attr('src', playAudio[p]['mp3']);
				sourceMp3.attr('type', 'audio/mpeg');
				audio.append(sourceMp3);
				
				var sourceOgg = $('<source>');
				
				sourceOgg.attr('src', playAudio[p]['ogg']);
				sourceOgg.attr('type', 'audio/ogg');
				audio.append(sourceOgg);
				
				audio[0].load();
				audioPreload.append(audio);
			}
		});
	})(jQuery);
</script>

OUTPUT;

		echo $output;
	}

	public function form( $instance ) {

	}

	public function update( $new_instance, $old_instance ) {

	}

	private function filterAudioJson( $attachments, $key = null ) {
		$json = json_encode( array() );

		if ( !empty( $key ) ) {
			$posts = array_filter( $attachments, function ( $attachment ) use ( $key ) {
				return ( strpos( $attachment->post_title, $key ) !== false );
			});
		} else {
			$posts = $attachments;
		}

		$files = array();

		foreach ( $posts as $post ) {
			$attachment_url = wp_get_attachment_url( $post->ID );
			$attachment_info = parse_url( $attachment_url );
			$attachment_file = $attachment_info['path'];

			$filename_base = null;
			$mime_type_key = null;

			switch( $post->post_mime_type ) {
				case 'audio/ogg':
					$filename_base = basename( $attachment_file, '.ogg' );
					$mime_type_key = 'ogg';
					break;
				case 'audio/mpeg':
				default:
					$filename_base = basename( $attachment_file, '.mp3' );
					$mime_type_key = 'mp3';
			}

			$files[ $filename_base ][ $mime_type_key ] = $attachment_file;
		}

		if ( !empty( $files ) ) {
			$files = array_values( $files ) ;

			$json = json_encode( $files );
		}

		return $json;
	}

	private function getRingtoneSetlist() {

		// Determine which variation to retrieve.
		$variation = mt_rand(1, 4);

		// Build the setlist.

		$ringtone_setlist = array();
		$chordset = $c_setlist = $e_flat_setlist = range(1, 6 );

		shuffle( $c_setlist );
		shuffle( $e_flat_setlist );

		$c_offset = 0;
		$e_flat_offset = 0;
		foreach ( $chordset as $chord ) {

			if ( $chord == 1 || $chord == 4 || $chord == 5 ) {
				$c_setlist_slice = array_slice( $c_setlist, $c_offset, 2 );
				$melodies = $c_setlist_slice;
				$setlist = 'c';
				$c_offset += 2;
			} else {
				$e_flat_setlist_slice = array_slice( $e_flat_setlist, $e_flat_offset, 2 );
				$melodies = $e_flat_setlist_slice;
				$setlist = 'e-flat';
				$e_flat_offset += 2;
			}

			foreach ( $melodies as $melody ) {
				$prefix = array(
					sprintf( '%02d', $chord ),
					sprintf( '%02d', $melody ),
					sprintf( '%02d', $variation ),
				);
				$ringtone_setlist[] = implode( '_', $prefix );
			}

			$ringtone_setlist_count = count( $ringtone_setlist );

			$floor = ( $ringtone_setlist_count < 3 ) ? 0 : $ringtone_setlist_count - 2;
			$noise_position = mt_rand( $floor, $ringtone_setlist_count );

			array_splice( $ringtone_setlist, $noise_position, 0 , array( 'noise' ) );
		}

		return $ringtone_setlist;
	}

	private function getSetlistFiles( $attachments, $setlist ) {

		$noise_files = array_filter( $attachments, function ( $attachment ) {
			return ( strpos( $attachment->post_name, '00_') !== false );
		} );

		shuffle( $noise_files );

		$setlist_files = array();

		foreach ( $setlist as $prefix ) {

			if ( $prefix == "noise" ) {

				$setlist_files[] = array_pop( $noise_files );

			} else {
				$posts = array_filter( $attachments, function ( $attachment ) use ( $prefix ) {
					return ( is_int( strpos( $attachment->post_name, $prefix ) ) );
				} );

				if ( !empty( $posts ) ) {
					$setlist_files[] = array_pop( $posts );
				}
			}
		}

		return $setlist_files;
	}
}