<?php
/**
 * Copyright (c) 2017 Observant Records.
 */

/**
 * Created by PhpStorm.
 * User: gregbueno
 * Date: 12/10/17
 * Time: 9:40 PM
 */

namespace ObservantRecords\WordPress\Plugins\MobileMusic\Push\Views;

/**
 * BaseView provides basic template rendering.
 *
 * @package ObservantRecords\WordPress\Plugins\MobileMusic\Push\Views
 * @author Greg Bueno
 */
class BaseView
{

	/**
	 * BaseView constructor.
	 */
	public function __construct()
	{
	}

	/**
	 * Displays a view with data.
	 *
	 * render() displays a view template with the specified data. If a base template directory is not specified,
	 * the Views directory is used.
	 *
	 * @since 1.0.0
	 *
	 * @param string $template_path The path to the template
	 * @param array $data Data to pass to the template
	 * @param string $template_dir The base template directory
	 * @return boolean
	 * @throws \Exception
	 */
	public static function render($template_path, $data = null, $template_dir = null ) {

		if ( !empty( $data ) ) {
			extract( $data );
		}

		if ( empty( $template_dir ) ) {
			$template_dir = plugin_dir_path( __FILE__ );
		}

		if ( empty( $template_path ) ) {
			throw new \Exception('$template_path cannot be empty or null');
		}

		if ( $result = file_exists( $template_dir . $template_path ) ) {
			include_once $template_dir . $template_path;
		}

		return $result;
	}

}