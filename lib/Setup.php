<?php
namespace ObservantRecords\WordPress\Plugins\MobileMusic\Push;

/**
 * Use this constant where plugin functions require scope.
 *
 * @since 1.0.0
 *
 */
const WP_PLUGIN_DOMAIN = 'empty_ensemble_mobile_music_push';

/**
 * Common functions for WordPress plugin setup and installation.
 *
 * Setup includes methods to fire when this plugin is installed, activated or deactivated. Use the init() method
 * to register plugin actions.
 *
 * @since 1.0.0
 *
 * @package ObservantRecords\WordPress\Plugins\MobileMusic\Push
 * @author Greg Bueno
 * @copyright Observant Records
 */
class Setup
{
	/**
	 * Setup constructor.
	 */
	public function __construct() {

	}

	/**
	 * init
	 *
	 * init() registers WordPress actions and filters to setup the plugin.
	 */
	public static function init() {
		add_action( 'init', array( __CLASS__, 'menus_init' ) );

		add_action( 'widgets_init', array( __CLASS__, 'widgets_init' ) );

		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'wp_enqueue_scripts' ), 22 );

		add_shortcode( 'mobile_music_push', __NAMESPACE__ . '\Shortcode::display' );
	}

	public static function wp_enqueue_scripts() {
		wp_enqueue_script( 'push', plugin_dir_url( dirname( dirname( __FILE__ ) ) . '/mobile-music-push.php' ) . "/assets/js/push.js", array( "jquery" ), '0.1.0b', true );
	}

	public static function widgets_init() {
		register_widget( __NAMESPACE__ . '\Widgets\Push' );

		register_sidebar( array(
			'name' => __( 'Mobile Music Instruments', WP_PLUGIN_DOMAIN ),
			'id' => 'sidebar-mobile-music-instrument',
			'description' => __( 'Side bar for Mobile Music Instruments', WP_PLUGIN_DOMAIN ),
		));
	}

	public static function menus_init() {
		register_nav_menu('mobile-music', 'Mobile Music');
	}

	/**
	 * activate
	 *
	 * Use this method with register_activation_hook().
	 */
	public static function activate() {

	}

	/**
	 * deactivate
	 *
	 * Use this method with register_deactivation_hook.
	 */
	public static function deactivate() {

	}

	/**
	 * uninstall
	 *
	 * Use this method with register_uninstall_hook().
	 */
	public static function uninstall() {

	}

}