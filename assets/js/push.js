/*
 * Copyright (c) 2017 Observant Records.
 */

(function ($) {
	$(function () {
		var PushAudio = {
			'play': function (audio) {
				var vibrateDone;
				if ("vibrate" in navigator) {
					vibrateDone = window.navigator.vibrate(20);
				}

				var status = $('<div>');

				if (arguments.length > 1) {
					status = arguments[1];
				}

				status.html("Stopping audio.");
				audio.pause();
				audio.currentTime = 0;

				status.html("Playing audio.");
				audio.play();

				status.html("Audio done.");
			},
			'playButton': {
				'counter': 0,
				'delay': 0,
				'control': null,
				'button': null,
				'currentMp3Source': null,
				'currentOggSource': null,
				'sourceFiles': null,
				'init': function (event) {
					this.control = $('.audio-play')[0];
					this.status = $('#status');
					this.button = $('.btn-play');
					this.currentMp3Source = $('.play-mp3');
					this.currentOggSource = $('.play-ogg');
					this.sourceFiles = (typeof playAudio !== 'undefined') ? playAudio : [];
					this.load();
				},
				'play': function (event) {
					PushAudio.play(this.control);

					this.button.attr('disabled', 'disabled');
					this.status.html('Please wait. Time remaining: <span class="time-left"></span>');

					var testButton = PushAudio.testButton.button;
					testButton.attr('disabled', 'disabled');

					var calculatedDelay = this.delay + Math.ceil(this.control.duration);

					if (calculatedDelay > 15) {
						calculatedDelay = 1;
					}

					this.delay = (calculatedDelay > this.control.duration) ? calculatedDelay : this.control.duration;

					var displaySeconds = Math.round(this.delay);

					$('.time-left').html(displaySeconds);

					var clock = setInterval(function () {
						displaySeconds--;
						$('.time-left').html(displaySeconds);
					}, 1000);

					setTimeout(function () {
						PushAudio.playButton.counter++;

						if (PushAudio.playButton.counter >= PushAudio.playButton.sourceFiles.length) {
							PushAudio.playButton.status.html('You have reached the end of the piece.');
							PushAudio.playButton.button.removeClass('btn-primary').addClass('btn-default');

							PushAudio.testButton.button.attr('disabled', 'disabled');

							return false;
						}

						PushAudio.playButton.button.attr('disabled', null);
						PushAudio.playButton.status.html('');

						PushAudio.playButton.load();

						clearInterval(clock);

					}, this.delay * 1000 );

				},
				'load': function (event) {
					this.currentMp3Source.attr('src', this.sourceFiles[this.counter]['mp3']);
					this.currentOggSource.attr('src', this.sourceFiles[this.counter]['ogg']);
					if (typeof this.control !== 'undefined') {
						this.control.load();
					}
				}
			},
			'testButton': {
				'counter': 0,
				'control': null,
				'button': null,
				'currentMp3Source': null,
				'currentOggSource': null,
				'sourceFiles': null,
				'init': function (event) {
					this.control = $('.audio-test')[0];
					this.status = $('#status');
					this.button = $('.btn-test');
					this.currentMp3Source = $('.test-mp3');
					this.currentOggSource = $('.test-ogg');
					this.sourceFiles = (typeof playAudio !== 'undefined') ? playAudio : [];
					this.load();
				},
				'play': function (event) {
					PushAudio.play(this.control);

					this.button.attr('disabled', 'disabled');
					this.status.html('Please wait. Time remaining: <span class="time-left"></span>');

					var displaySeconds = (this.control.duration > 3 ) ? 3 : Math.ceil(this.control.duration);

					$('.time-left').html(displaySeconds);

					var clock = setInterval(function () {
						displaySeconds--;
						$('.time-left').html(displaySeconds);
					}, 1000);

					setTimeout(function () {
						PushAudio.testButton.control.pause();
						PushAudio.testButton.control.currentTime = 0;

						PushAudio.testButton.button.attr('disabled', null);
						PushAudio.testButton.status.html('');

						clearInterval(clock);

					}, displaySeconds * 1000 );
				},
				'load': function (event) {
					this.currentMp3Source.attr('src', this.sourceFiles[this.counter]['mp3']);
					this.currentOggSource.attr('src', this.sourceFiles[this.counter]['ogg']);
					if (typeof this.control !== 'undefined') {
						this.control.load();
					}
				}
			}
		};

		var btnPlay = $(".btn-play");

		PushAudio.playButton.init();

		btnPlay.click(function () {
			PushAudio.playButton.play();
		});

		btnPlay.bind("touchend", function (event) {
			// event.preventDefault();
			PushAudio.playButton.play();
		});

		var btnTest = $(".btn-test");

		PushAudio.testButton.init();

		btnTest.click(function () {
			PushAudio.testButton.play();
		});

		btnTest.bind("touchend", function (event) {
			// event.preventDefault();
			PushAudio.testButton.play();
		});
	});
})(jQuery);
